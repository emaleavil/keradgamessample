package com.eeema.keradgametestapp.utils;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterSession;

/**
 * Created by emanuel on 16/02/16.
 */
public class Utils {

    public static TwitterSession getTwitterSession(){
        return Twitter.getSessionManager().getActiveSession();
    }

    public static String getTwitterToken(){
        return getTwitterSession().getAuthToken().token;
    }

    public static String getTwitterSecret(){
        return getTwitterSession().getAuthToken().secret;
    }
}
