package com.eeema.keradgametestapp.repository;

import rx.Observer;

/**
 * Created by emanuel on 16/02/16.
 */
public interface Repository {

    void login(Observer subscriber);
    void search(String query);
}
