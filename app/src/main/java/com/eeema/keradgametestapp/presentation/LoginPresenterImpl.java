package com.eeema.keradgametestapp.presentation;

import com.eeema.keradgametestapp.app.view.LoginView;

/**
 * Created by emanuel on 16/02/16.
 */
public class LoginPresenterImpl implements LoginPresenter {

    LoginView view;

    public LoginPresenterImpl(LoginView view){
        this.view = view;
    }


    @Override
    public void sendUserDetails(long userId, String userName) {
        view.fillUserDetails(userId,userName);
    }

    @Override
    public void success() {
        view.success();
    }

    @Override
    public void error(String error) {
        view.error(error);
    }

}
