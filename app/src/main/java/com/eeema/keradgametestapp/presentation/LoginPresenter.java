package com.eeema.keradgametestapp.presentation;

/**
 * Created by emanuel on 16/02/16.
 */
public interface LoginPresenter {

    void sendUserDetails(long userId, String userName);
    void success();
    void error(String error);
}
