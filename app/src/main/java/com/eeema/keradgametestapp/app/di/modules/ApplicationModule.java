package com.eeema.keradgametestapp.app.di.modules;

import android.content.Context;

import com.eeema.keradgametestapp.datasources.RepositoryImpl;
import com.eeema.keradgametestapp.repository.Repository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by emanuel on 16/02/16.
 */
@Singleton
@Module
public class ApplicationModule {

    private Context appContext;

    public ApplicationModule(Context appContext){
        this.appContext = appContext;
    }

    @Provides
    @Singleton
    public Context provideAppContext(){
        return appContext;
    }

    @Provides @Singleton
    Repository provideRepository(){
        return new RepositoryImpl();
    }

}
