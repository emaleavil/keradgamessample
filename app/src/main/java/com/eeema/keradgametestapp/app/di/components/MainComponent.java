package com.eeema.keradgametestapp.app.di.components;


import com.eeema.keradgametestapp.app.activities.MainActivity;
import com.eeema.keradgametestapp.app.di.PerActivity;
import com.eeema.keradgametestapp.app.di.modules.MainModule;

import dagger.Component;

/**
 * Created by emanuel on 7/12/15.
 */

@PerActivity
@Component(dependencies = {ApplicationComponent.class}, modules = {MainModule.class})
public interface MainComponent {
    void inject(MainActivity mainActivity);
}
