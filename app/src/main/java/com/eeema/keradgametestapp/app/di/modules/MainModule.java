package com.eeema.keradgametestapp.app.di.modules;

import com.eeema.keradgametestapp.app.activities.MainActivity;

import dagger.Module;

/**
 * Created by emanuel on 7/12/15.
 */
@Module
public class MainModule {

    private MainActivity mainActivity;

    public MainModule(MainActivity activity){
        this.mainActivity = activity;
    }

}

