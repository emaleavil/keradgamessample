package com.eeema.keradgametestapp.app.view;

/**
 * Created by emanuel on 16/02/16.
 */
public interface LoginView {

    void success();
    void error(String error);
    void fillUserDetails(long userId, String userName);
}
