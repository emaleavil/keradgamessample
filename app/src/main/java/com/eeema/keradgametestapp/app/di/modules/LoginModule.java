package com.eeema.keradgametestapp.app.di.modules;

import com.eeema.keradgametestapp.app.activities.LoginActivity;
import com.eeema.keradgametestapp.app.listeners.LoginCallback;
import com.eeema.keradgametestapp.app.view.LoginView;
import com.eeema.keradgametestapp.presentation.LoginPresenter;
import com.eeema.keradgametestapp.presentation.LoginPresenterImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by emanuel on 16/02/16.
 */
@Module
public class LoginModule {

    private LoginActivity activity;

    public LoginModule(LoginActivity activity){
        this.activity = activity;
    }

    @Provides
    public LoginView provideView(){
        return activity;
    }

    @Provides
    public LoginPresenter providePresenter(){
        return new LoginPresenterImpl(activity);
    }

     @Provides
    public LoginCallback provideCallback(LoginPresenter presenter){
         return new LoginCallback(presenter);
     }
}
