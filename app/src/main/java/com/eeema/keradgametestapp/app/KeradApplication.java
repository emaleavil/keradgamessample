package com.eeema.keradgametestapp.app;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.eeema.keradgametestapp.app.di.components.ApplicationComponent;
import com.eeema.keradgametestapp.app.di.components.DaggerApplicationComponent;
import com.eeema.keradgametestapp.app.di.modules.ApplicationModule;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import io.fabric.sdk.android.Fabric;

/**
 * Created by emanuel on 16/02/16.
 */
public class KeradApplication extends Application {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    //TODO put in the correct place
    private static final String TWITTER_KEY = "PWhHyg7h8MGCzKM3nrBa6MnsL";
    private static final String TWITTER_SECRET = "gIimaWxVtGsidbJrbuedANkbqjjWBXIWX9cJEzDAtuTNvApQGy";

    private static KeradApplication sInstance = null;

    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        initTwitter();
        sInstance = this;

        component  = DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(this)).build();
        component.inject(sInstance);
    }

    public static KeradApplication getsInstance(){
        return sInstance;
    }

    public ApplicationComponent getApplicationComponent(){
        return component;
    }

    private void initTwitter(){
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Crashlytics(), new Twitter(authConfig));
    }

}
