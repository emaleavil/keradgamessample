package com.eeema.keradgametestapp.app.listeners;

import com.eeema.keradgametestapp.presentation.LoginPresenter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;

/**
 * Created by emanuel on 16/02/16.
 */
public class LoginCallback extends Callback<TwitterSession>{

    private LoginPresenter presenter;

    public LoginCallback(LoginPresenter presenter){
        this.presenter = presenter;
    }

    @Override
    public void success(Result<TwitterSession> result) {
        //TODO manage result data
        presenter.sendUserDetails(result.data.getUserId(), result.data.getUserName());
        presenter.success();
    }

    @Override
    public void failure(TwitterException e) {
        //TODO create friendly message
        presenter.error(e.getMessage());
    }
}
