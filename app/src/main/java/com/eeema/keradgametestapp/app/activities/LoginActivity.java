package com.eeema.keradgametestapp.app.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;

import com.eeema.keradgametestapp.R;
import com.eeema.keradgametestapp.app.KeradApplication;
import com.eeema.keradgametestapp.app.di.components.DaggerLoginComponent;
import com.eeema.keradgametestapp.app.di.modules.LoginModule;
import com.eeema.keradgametestapp.app.listeners.LoginCallback;
import com.eeema.keradgametestapp.app.view.LoginView;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by emanuel on 16/02/16.
 */
public class LoginActivity extends AppCompatActivity implements LoginView {



    @Bind(R.id.login_button)
    TwitterLoginButton loginButton;
    @Bind(R.id.parent)
    RelativeLayout parentView;

    @Inject
    LoginCallback loginCallback;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initButterKnife();
        initDagger();
        setListeners();
    }

    private void initButterKnife() {
        ButterKnife.bind(this);
    }


    private void initDagger() {
        DaggerLoginComponent.builder()
                .loginModule(new LoginModule(this))
                .applicationComponent(KeradApplication.getsInstance().getApplicationComponent())
                .build()
                .inject(this);
    }



    private void setListeners(){
        loginButton.setCallback(loginCallback);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        loginButton.onActivityResult(requestCode,resultCode,data);
    }

    @Override
    public void success() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void error(String error) {
        Snackbar snackbar = Snackbar.make(parentView, error, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    @Override
    public void fillUserDetails(long userId, String userName) {
        //TODO do it something with this data
    }
}
