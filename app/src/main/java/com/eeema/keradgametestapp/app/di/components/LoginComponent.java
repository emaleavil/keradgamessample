package com.eeema.keradgametestapp.app.di.components;

import com.eeema.keradgametestapp.app.activities.LoginActivity;
import com.eeema.keradgametestapp.app.di.PerActivity;
import com.eeema.keradgametestapp.app.di.modules.LoginModule;

import dagger.Component;

/**
 * Created by emanuel on 16/02/16.
 */
@PerActivity
@Component(dependencies = {ApplicationComponent.class}, modules = {LoginModule.class})
public interface LoginComponent {
    void inject(LoginActivity activity);
}