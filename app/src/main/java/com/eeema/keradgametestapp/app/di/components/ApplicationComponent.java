package com.eeema.keradgametestapp.app.di.components;

import android.content.Context;

import com.eeema.keradgametestapp.app.di.modules.ApplicationModule;
import com.eeema.keradgametestapp.repository.Repository;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by emanuel on 7/12/15.
 */

@Singleton
@Component(modules =
        {
                ApplicationModule.class
        })
public interface ApplicationComponent {

    void inject(Context context);

    Context context();
    Repository getRepository();
}
