package com.eeema.keradgametestapp.domain.interactors;

import com.eeema.keradgametestapp.repository.Repository;

import javax.inject.Inject;

import rx.Observer;

/**
 * Created by emanuel on 16/02/16.
 */
public class LoginAction implements Invoker {

    @Inject
    Repository repository;

    @Override
    public void execute(Observer subscriber) {
        repository.login(subscriber);
    }
}
