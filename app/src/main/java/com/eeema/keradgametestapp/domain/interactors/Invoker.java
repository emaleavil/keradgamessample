package com.eeema.keradgametestapp.domain.interactors;

import rx.Observer;

/**
 * Created by Emanuel on 26-10-2015.
 */
public interface Invoker {
    void execute(Observer subscriber);
}
